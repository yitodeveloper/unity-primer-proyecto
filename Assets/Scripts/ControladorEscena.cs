﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControladorEscena : MonoBehaviour {

    public void cambiarEscena(string nombre) {
        print(nombre);
        SceneManager.LoadScene(nombre);
    }

    public void salir() {
        Application.Quit();
    }
}
