﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.BLL
{
    public class Diapositiva
    {
        public String titulo;
        public String descripcion;
        public Sprite fotografia;

        public Diapositiva(String titulo, String descripcion, String fotografia) {
            this.titulo = titulo;
            this.descripcion = descripcion;
            this.fotografia = Resources.Load<Sprite>(fotografia);
        }
    }
}
