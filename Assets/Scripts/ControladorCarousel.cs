﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.BLL;

public class ControladorCarousel : MonoBehaviour {

    public Text titulo;
    public Text Descripcion;
    public Image fotografia;
    public int contador = 0;

    public List <Diapositiva> diapositivas = new List <Diapositiva> ();

	// Use this for initialization
	void Start () {
        //Resources.Load<Sprite>("Imagenes/01");
        diapositivas.Add(new Diapositiva("Bienvenido", "Mi primer Jueguito", "Imagenes/01"));
        diapositivas.Add(new Diapositiva("Segunda Imagen", "Que bkn es programar", "Imagenes/02"));
        diapositivas.Add(new Diapositiva("Tercera Imagen", "Que Awesome es programar", "Imagenes/03"));
        diapositivas.Add(new Diapositiva("Cuarta Imagen", "Que cool es programar", "Imagenes/04"));

        cambiarDiapositiva(diapositivas[0]);
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.RightArrow)) {
            contador++;
            if (contador >= 4) {
                contador = 0;
            }
            cambiarDiapositiva(diapositivas[contador]);
        } else if (Input.GetKeyDown(KeyCode.LeftArrow)) {
            contador--;
            if (contador < 0) {
                contador = 3;
            }
            cambiarDiapositiva(diapositivas[contador]);
        }
	}

    private void cambiarDiapositiva(Diapositiva diapo) {
        titulo.text = diapo.titulo;
        Descripcion.text = diapo.descripcion;
        fotografia.sprite = diapo.fotografia;
    }
}
